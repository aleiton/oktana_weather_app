import { combineReducers } from 'redux';

import chartReducer from './chartReducer';
import favoritesReducer from './favoritesReducer';

export default combineReducers({
  chart: chartReducer,
  favorites: favoritesReducer
});