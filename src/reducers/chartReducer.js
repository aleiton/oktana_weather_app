import moment from 'moment';

import { REQUEST_CHART, RECEIVE_CHART, SELECT_CHART, CLEAN_CHART } from '../actions/types';

const INITIAL_STATE = {
  weather: {
    celcius: [],
    fahrenheit: [],
    humidity: [],
    pressure: []
  },
  city: {},
  type: 'celcius',
  isFetching: false,
  didInvalidate: false
}

export default (state = INITIAL_STATE, {type, payload }) => {
  switch (type) {
    case REQUEST_CHART:
      return {...state, isFetching: true };
    case RECEIVE_CHART:
      if (payload.response && payload.response.status === 404) {
        return { ...INITIAL_STATE, didInvalidate: true }
      }
      const celcius = [];
      const fahrenheit = [];
      const humidity = [];
      const pressure = [];
      const city = payload.city;
      payload.list.forEach(({ main, dt_txt }) => {
        const formated_dt_txt = moment(dt_txt, 'YYYY-MM-DD hh:mm:ss').calendar();
        celcius.push({ celcius: Math.trunc(main.temp-273.15) , name: formated_dt_txt });
        fahrenheit.push({ fahrenheit: Math.trunc(((main.temp-273.15)*1.8)+32), name: formated_dt_txt });
        humidity.push({ humidity: main.humidity, name: formated_dt_txt });
        pressure.push({ pressure: main.pressure, name: formated_dt_txt });
      });
      return {
        ...state,
        weather: {
          celcius,
          fahrenheit,
          humidity,
          pressure
        },
        city,
        isFetching: false,
        didInvalidate: false
      };
    case SELECT_CHART:
      return {...state, type: payload };
    case CLEAN_CHART:
      return INITIAL_STATE;
    default:
      return state;
  }
};