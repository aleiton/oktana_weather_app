import { ADD_FAVORITE, REMOVE_FAVORITE } from '../actions/types';

export default (state = [], action) => {
  switch (action.type) {
    case ADD_FAVORITE:
      return [...state, action.city ];
    case REMOVE_FAVORITE:
      return state.filter(element => element.id !== action.cityId);
    default:
      return state;
  }
};