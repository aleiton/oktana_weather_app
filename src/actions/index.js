import weather from '../apis/weather';
import {
  ADD_FAVORITE,
  REMOVE_FAVORITE,
  REQUEST_CHART,
  RECEIVE_CHART,
  SELECT_CHART,
  CLEAN_CHART
} from './types';

  //** Favorites */

  export const addFavorite = city => {
    return {
      type: ADD_FAVORITE,
      city
    }
  };

  export const removeFavorite = cityId => {
    return {
      type: REMOVE_FAVORITE,
      cityId
    };
  };

  //** Chart */

  export const requestChart = () => {
      return {
        type: REQUEST_CHART,
      };
    };

  export const receiveChart = term => async dispatch => {
      try {
        const response = await weather.get('/forecast', {
          params: {
            q: term
          }
        });
        dispatch({
          type: RECEIVE_CHART,
          payload: response.data
        });
      } catch(err) {
        dispatch({
          type: RECEIVE_CHART,
          payload: err
        });

      }

  };

  
  export const selectChart = chart => {
    return {
      type: SELECT_CHART,
      payload: chart
    };
  };

  export const cleanChart = () => {
    return {
      type: CLEAN_CHART
    };
};