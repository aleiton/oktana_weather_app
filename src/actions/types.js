export const ADD_FAVORITE    = 'ADD_FAVORITE';
export const REMOVE_FAVORITE = 'REMOVE_FAVORITE';
export const REQUEST_CHART   = 'REQUEST_CHART';
export const RECEIVE_CHART   = 'RECEIVE_CHART';
export const SELECT_CHART    = 'SELECT_TYPE';
export const CLEAN_CHART     = 'CLEAN_CHART';
