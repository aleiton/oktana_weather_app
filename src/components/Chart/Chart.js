import React from 'react';
import { connect } from 'react-redux';
import { LineChart, Line, CartesianGrid, XAxis, YAxis, Tooltip, ResponsiveContainer } from 'recharts';

const Chart = ({ weather, type, receiveChart }) => {
  return (
  <ResponsiveContainer width="100%" height={500}>
    <LineChart data={weather[type]} margin={{ top: 5, right: 20, bottom: 5, left: 0 }}>
      <Line type="monotone" dataKey={type} stroke="#8884d8" />
      <CartesianGrid stroke="#ccc" strokeDasharray="5 5" />
      <XAxis dataKey="name" />
      <YAxis />
      <Tooltip />
    </LineChart>
  </ResponsiveContainer>
  );
};

const mapStateToProps = ({ chart: { weather, type } }) => {
  return { weather, type };
};

export default connect(mapStateToProps)(Chart);
