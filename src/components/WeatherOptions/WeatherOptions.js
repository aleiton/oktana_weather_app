import React from 'react';
import { connect } from 'react-redux';
import { addFavorite, removeFavorite, selectChart } from '../../actions/index';

class WeatherOptions extends React.Component {

  onTypeClick = event => {
    switch(event.target.value) {
      case 'temperature':
          this.props.selectChart('celcius');
        break;
      case 'humidity':
        this.props.selectChart('humidity');
        break;
      case 'pressure':
        this.props.selectChart('pressure');
        break;
      default:
        break;
    }
  }

  onScaleClick = event => {
    switch(event.target.value)  {
      case 'C':
        this.props.selectChart('celcius');
        break;
      case 'F':
        this.props.selectChart('fahrenheit')
        break;
      default:
        break;
    }
  }

  toggleFavorite = () => {
    const isFavorite = (this.props.favorites.find(city => city.id === this.props.city.id) !== undefined) ? true : false;
    if (isFavorite) {
      this.props.removeFavorite( this.props.city.id );
    } else {
      this.props.addFavorite( this.props.city );
    }
  }

  render() {
    const currentCity = this.props.city;
    const favorites = this.props.favorites;
    const type = this.props.type;
    const isFavorite = (favorites.find(city => city.id === currentCity.id) !== undefined) ? true : false;
    return(
      <div>
        <div className="ui vertical buttons">
          <div className="ui vertical buttons">
            <button onClick={this.onTypeClick} value='temperature' className={`ui ${(type === 'celcius' || type === 'fahrenheit') ? 'active' : ''} basic button`}>Temperature</button>
            <div className="ui vertical basic buttons" style={{ display: ((type === 'celcius' || type === 'fahrenheit') ? '' : 'none')  }}>
              <button onClick={this.onScaleClick} value='C' className={`ui ${(type === 'celcius' ? 'active' : '')} button`}>ºC</button>
              <button onClick={this.onScaleClick} value='F' className={`ui ${type === 'fahrenheit' ? 'active' : ''} button`}>ºF</button>
            </div>
          </div>
          <button onClick={this.onTypeClick} value='humidity' className={`ui ${(type === 'humidity') ? 'active' : ''} basic button`}>Humidity</button>
          <button onClick={this.onTypeClick} value='pressure' className={`ui ${(type === 'pressure') ? 'active' : ''} basic button`}>Pressure</button>
        </div>
        <div className="center aligned segment" style={{ cursor: 'pointer' }}>
          <i onClick={this.toggleFavorite} className={`big yellow star ${isFavorite ? '' : 'outline'} icon`}></i>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ favorites, chart: { city, weather, type } }) => {
  return { favorites, city, weather, type }; 
}

export default connect(mapStateToProps, { addFavorite, removeFavorite, selectChart })(WeatherOptions);