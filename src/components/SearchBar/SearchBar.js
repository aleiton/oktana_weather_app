import React from 'react';
import { withRouter } from 'react-router-dom';

class SearchBar extends React.Component {
  state = {term: '' };

  /** Callbacks Handlers */

  onInputChange = event => {
    this.setState({ term: event.target.value });
  };

  onFormSubmit = event => {
    event.preventDefault();
    //this.props.history.push(`/home/${this.state.term}`, { term: this.state.term });
    this.props.history.push(`/`, { term: this.state.term });
  };

  render() {
    return (
      <div className="search-bar ui segment">
        <form onSubmit={this.onFormSubmit} className="ui form">
          <div className="field">
            <label>Search a city</label>
            <input
              type="text"
              value={this.state.term}
              placeholder="e.g. New York, US"
              onChange={this.onInputChange}
              required
            />
          </div>
        </form>
      </div>
    );
  };
}

export default withRouter(SearchBar);