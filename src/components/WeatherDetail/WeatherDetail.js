import React from 'react';
import { connect } from 'react-redux';

import Chart from '../Chart/Chart';
import WeatherOptions from '../WeatherOptions/WeatherOptions';


class WeatherDetail extends React.Component {

  render() {
    const { weather, city, type, isFetching, didInvalidate } = this.props;
    if (!isFetching && !didInvalidate && weather.celcius.length === 0 ) {
      return null;
    }

    if (isFetching) {
      return(
        <div className="ui container">
          <div className="ui active inverted dimmer">
            <div className="ui text loader">Loading</div>
          </div>
          <p></p>
        </div>
      );
    }

    if (didInvalidate) {
      return (
        <div className="ui big negative message">
          <div className="header">
            City was not found
          </div>
          <p>Check if the format of your search correct, for example: <b>New York, US</b></p>
        </div>
      );
    }

    return (
      <div className="ui stackable grid">
        <div className="ui row">
          <div className="sizteen wide column">
            <div className="ui center aligned container">
              <h3>{city.name} - {city.country}</h3>
              <h5>{type}</h5>
            </div>
          </div>
        </div>
        <div className="ui row center aligned">
          <div className="two wide column">
            <WeatherOptions />
          </div>
          <div className="fourteen wide column">
            <Chart />
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToPops = ({chart: {weather, city, type, isFetching, didInvalidate }}) => {
  return { weather, city, type, isFetching, didInvalidate };
}

export default connect(mapStateToPops)(WeatherDetail);