import './Footer.css';
import React from 'react';

const Footer = () => {
  return (
  <div id="footer" className="ui left aligned footer container">
    <h5>Designed by Alejandro Leiton</h5>
  </div>
  );
};

export default Footer;