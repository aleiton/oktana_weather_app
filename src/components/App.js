import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';

import Header from './Header/Header';
import Footer from './Footer/Footer';
import Home from './Home/Home';
import FavoriteList from './FavoriteList/FavoriteList';

const App = () => {
  return (
    <BrowserRouter>
      <Header />
      <div className="ui container">
        <Route path="/" exact component={Home} />
        <Route path="/favorites" exact component={FavoriteList} />
      </div> 
      <Footer />
    </BrowserRouter>
  );
};

export default App;