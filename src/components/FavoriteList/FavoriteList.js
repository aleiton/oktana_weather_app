import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

class FavoriteList extends React.Component {

  renderList() {
    return this.props.favorites.map(city => {
      return (
        <div className="item" key={city.id}>
          <div className="content">
            <div className="description">
              <Link to={{ pathname: '/', state: { term: `${city.name}, ${city.country}` }}} >
                <h3>{city.name} - {city.country} </h3>
              </Link>
            </div>
          </div>
        </div>
      );
    });
  };

  render() {
    if (this.props.favorites.length === 0) {
      return(
        <div>
          <div className="ui header">Favorites</div>
          <div className="ui center aligned container">
            <h2>There are no favorites added</h2>
            <p>Add your favorites cities to keep track!</p>
          </div>
        </div>
      );
    }

    return (
      <div>
        <div className="ui header">Favorites</div>
        <div className="ui relaxed divided list">{this.renderList()}</div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return { favorites: state.favorites }
}

export default connect(mapStateToProps)(FavoriteList);