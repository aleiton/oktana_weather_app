import React from 'react';
import { connect } from 'react-redux';
import { receiveChart, requestChart, cleanChart } from '../../actions/index';

import SearchBar from '../SearchBar/SearchBar';
import WeatherDetail from '../WeatherDetail/WeatherDetail';

class Home extends React.Component {

  render() {
    const state = this.props.history.location.state
    if (state !== undefined) {
      this.props.requestChart();
      this.props.receiveChart(state.term);
    } else {
      this.props.cleanChart();
    }

    return(
      <div className="ui container">
        <SearchBar />
        <div className="ui divider"></div>
        <WeatherDetail />
      </div>
    )
  }
}

export default connect(null, { receiveChart, requestChart, cleanChart })(Home);