import React from 'react';
import { Link } from 'react-router-dom';

const Header = () => {
  return (
    <div className="ui inverted menu">
      <Link to="/" className="item">
        Oktana Weather App
      </Link>
      <div className="right menu">
        <Link to="/favorites" className="item">
          Favorites
        </Link>
      </div>
    </div>
  );
};

export default Header;