import axios from 'axios';

const KEY = '8eac48fdcc6d42a66763d865cc919f11';

export default axios.create({
  baseURL: 'https://api.openweathermap.org/data/2.5',
  params: {
    appid: KEY
  }
});